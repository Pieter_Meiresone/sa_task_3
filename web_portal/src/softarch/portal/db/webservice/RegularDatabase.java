package softarch.portal.db.webservice;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import librarysearch.soft.Book;
import librarysearch.soft.LibrarySearchRequest;
import librarysearch.soft.LibrarySearchResponse;
import librarysearch.soft.LibrarySearchServiceLocator;
import librarysearch.soft.LibrarySearchServicePortType;
import softarch.portal.data.RegularData;
import softarch.portal.db.RegularDatabaseInterface;
import softarch.portal.db.sql.DatabaseException;

public class RegularDatabase extends Database implements RegularDatabaseInterface {
	
	LibrarySearchServicePortType service;
	
	public RegularDatabase() {
		try {
			service = new LibrarySearchServiceLocator().getLibrarySearchServicePort(new URL("http://localhost:8080/ode/processes/LibrarySearchService"));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	public List findRecords(String informationType, String queryString)
			throws DatabaseException {
		
		try {
			if (informationType.equals("Book")) {				
				LibrarySearchRequest request = new LibrarySearchRequest();
				request.setQuery(queryString);
				LibrarySearchResponse response = service.process(request);
				Book[] books = response.getBooks();
				
				LinkedList<softarch.portal.data.Book> result = new LinkedList<softarch.portal.data.Book>();
				for(Book book : books) {
					result.add(convertBook(book));
				}
				return result;
			} else {
				// Other types are not supported.
				return new LinkedList();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DatabaseException(e.getMessage());
		}
	}

	public List findRecordsFrom(String informationType, Date date)
			throws DatabaseException {
		try {
			if (informationType.equals("Book")) {				
				LibrarySearchRequest request = new LibrarySearchRequest();
				request.setQuery("");
				LibrarySearchResponse response = service.process(request);
				Book[] books = response.getBooks();
				
				LinkedList<softarch.portal.data.Book> result = new LinkedList<softarch.portal.data.Book>();
				for(Book book : books) {
					result.add(convertBook(book));
				}
				return result;
			} else {
				// Other types are not supported.
				return new LinkedList();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DatabaseException(e.getMessage());
		}
	}

	public void add(RegularData rd) throws DatabaseException {
		throw new DatabaseException("Unsupported operation");
	}

	public int getNumberOfRegularRecords(String informationType)
			throws DatabaseException {
		try {
			if (informationType.equals("Book")) {				
				LibrarySearchRequest request = new LibrarySearchRequest();
				request.setQuery("");
				LibrarySearchResponse response = service.process(request);
				
				return response.getBooks().length;
			} else {
				// Other types are not supported.
				return 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DatabaseException(e.getMessage());
		}
	}
	
	public softarch.portal.data.Book convertBook(librarysearch.soft.Book input) {
		Date publicationDate = new Date();
		if (input.getPublicationDate() != null) {
			publicationDate = input.getPublicationDate().getTime();
		}
		softarch.portal.data.Book output = new softarch.portal.data.Book(
				input.getDate(), 
				input.getAuthor(), 
				input.getIsbn().longValue(), 
				input.getPages(), 
				publicationDate, 
				input.getPublisher(), 
				input.getReview(), 
				input.getSummary(), 
				input.getTitle());
		
		return output;
	}

}
