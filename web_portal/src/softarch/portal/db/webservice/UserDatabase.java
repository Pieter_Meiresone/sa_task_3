package softarch.portal.db.webservice;

import softarch.portal.data.UserProfile;
import softarch.portal.db.UserDatabaseInterface;
import softarch.portal.db.sql.DatabaseException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class UserDatabase extends Database implements UserDatabaseInterface {

	public void insertFree(UserProfile profile) throws DatabaseException {
		throw new NotImplementedException();
	}

	public void insertCheap(UserProfile profile) throws DatabaseException {
		throw new NotImplementedException();
	}

	public void insertExpensive(UserProfile profile) throws DatabaseException {
		throw new NotImplementedException();
	}

	public void updateFree(UserProfile profile) throws DatabaseException {
		throw new NotImplementedException();
	}

	public void updateCheap(UserProfile profile) throws DatabaseException {
		throw new NotImplementedException();
	}

	public void updateExpensive(UserProfile profile) throws DatabaseException {
		throw new NotImplementedException();
	}

	public UserProfile findUser(String username) throws DatabaseException {
		throw new NotImplementedException();
	}

	public boolean userExists(String username) throws DatabaseException {
		throw new NotImplementedException();
	}

}
