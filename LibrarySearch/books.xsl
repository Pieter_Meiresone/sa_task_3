<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:param name="second">
	foo
	</xsl:param>
 	<xsl:template match="/">
		<root>
		<xsl:apply-templates select="//book" />
		<xsl:for-each select="$second">
			<book>
				<xsl:apply-templates />
			</book>
		</xsl:for-each>
		</root>
	</xsl:template>
	
	<!-- standard copy template -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>	
</xsl:stylesheet>